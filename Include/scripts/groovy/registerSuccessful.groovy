import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.time.Instant

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class registerSuccessful {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I open application")
	def I_open_application() {
		Mobile.callTestCase(findTestCase('pages/open application'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I go to login page")
	def I_go_to_login_page() {
		Mobile.callTestCase(findTestCase('pages/products/go to login page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I go to register page")
	def I_go_to_register_page() {
		Mobile.callTestCase(findTestCase('pages/login/go to register page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I field (.*), (.*) and (.*) on register")
	def I_type_fullname_email_and_password(String fullname, String email, String password) {
		Mobile.callTestCase(findTestCase('pages/register/type fullname, email, password'), [('fullname') : fullname, ('email') : Instant.now().epochSecond+'@email.com', ('password'): password], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click register button")
	def I_click_register_button() {
		Mobile.callTestCase(findTestCase('pages/register/click register button'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}