#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Register feature
  I want to register account to buy products

  Scenario Outline: Register successful
    Given I open application
    When I go to login page
    When I go to register page
    When I field <fullname>, <email> and <password> on register
    When I click register button
    Then I close application

    Examples: 
      | fullname  | email 						| password  |
      | farhan 		| farhan@gmail.com 	| farhan |
  
  Scenario Outline: Register unsuccessful
    Given I open application
    When I go to login page
    When I go to register page
    When I click register button
    When I see gagal register message
    Then I close application